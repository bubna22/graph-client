package info.bubna.digraph.converter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Digraph6Converter {
    private static final Logger log = LoggerFactory.getLogger(Digraph6Converter.class);

    private Digraph6Converter() {
    }

    public static Integer[][] digraph6ToAdjMatrix(String digraph6) {
        log.trace(digraph6);
        var tmpDigraph6 = digraph6.substring(1);
        StringBuilder decodedAsciiDigraph6Str = new StringBuilder();
        var chars = tmpDigraph6.toCharArray();
        int verticesCount = chars[0] - 63;
        for (int j = 1; j < chars.length; j++) {
            var i = ((int) chars[j]) - 63;
            var bS = Integer.toBinaryString(i);
            var bRS = StringUtils.leftPad(bS, 6, '0');
            decodedAsciiDigraph6Str.append(bRS);
            log.trace("number - {}", bS);
            log.trace("binary number - {}", bRS);
        }
        var decodedStr = decodedAsciiDigraph6Str.toString();
        log.trace(decodedStr);
        char[] decodedStrChars = decodedStr.toCharArray();
        log.trace("decoded str chars - {}", new String(decodedStrChars));

        Integer[][] result = new Integer[verticesCount][];
        for (int i = 0; i < verticesCount; i++) {
            result[i] = new Integer[verticesCount];
            for (int j = 0; j < verticesCount; j++) {
                var c = Character.getNumericValue(decodedStrChars[i * verticesCount + j]);
                result[i][j] = c;
            }
        }

        log.trace("result adj matrix - {}", result);
        return result;
    }
}

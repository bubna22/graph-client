package info.bubna.graph.cycle;

import info.bubna.graph.cycle.generation.Graph6Generator;

public class Main {

    /**
     * Unit tests the {@code GraphGenerator} library.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        var res = Graph6Generator.graph6(5);
        System.out.println(res);
    }
}

package info.bubna.extension.vertex.math;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Min1ExtensionAlgorithm {

    private static final Logger log = LoggerFactory.getLogger(Min1ExtensionAlgorithm.class);

    public static class Vertex {
        public Integer num;//TODO remove
        public Integer out = 0;
        public Integer in = 0;
    }

    protected Min1ExtensionAlgorithm() {}

    @SuppressWarnings("ALL")
    public static Pair<Integer, Collection<Integer[][]>> min1Extension(Integer[][] adjInMatrix, boolean isVerticesExt) {
        ArrayList<Integer[][]> result = new ArrayList<>();

        log.trace("min1Extension isVerticesExt = {}", isVerticesExt);

        log.trace("min1Extension start");
        checkSquareMatrix(adjInMatrix);
        Integer[][] newAdjMatrix = null;

        newAdjMatrix = isVerticesExt ? addVertex(adjInMatrix) : adjInMatrix;
        log.trace("min1Extension newAdjMatrix - {}", newAdjMatrix);

        var verticesCount = adjInMatrix.length;
        var emptyPositions = findEmptyPositions(newAdjMatrix);
        log.trace("min1Extension findEmptyPositions - {}", emptyPositions);

        int kMax = isVerticesExt ? (verticesCount * verticesCount - verticesCount - 2) / 2
                : (verticesCount * verticesCount - 3 * verticesCount) / 2;
        log.trace("min1Extension kMax - {}", kMax);

        ArrayList<Vertex> vertices = new ArrayList<>();
        int edgesCount = 0;

        for (int i = 0; i < verticesCount; i++) {
            Vertex v = new Vertex();
            v.num = i;
            vertices.add(v);
        }

        for (int i = 0; i < verticesCount; i++)
            for (int j = 0; j < verticesCount; j++) {
                if (adjInMatrix[i][j] == 1) {
                    vertices.get(i).out++;
                    vertices.get(j).in++;
                    edgesCount++;
                }
            }

        var adjInMatrixVerticesDescription = Pair.of(vertices, edgesCount);
        log.trace("min1Extension adjInMatrixVerticesDescription - {}", adjInMatrixVerticesDescription);

        var kMinEdges = 0;
        for (kMinEdges = isVerticesExt ? 2 : 1; kMinEdges < kMax; kMinEdges++) {
            var resultPermutationList = new ArrayList<Integer[]>();
            permutation(emptyPositions, kMinEdges, 0, verticesCount, new Integer[kMinEdges], resultPermutationList);
            log.trace("min1Extension resultPermutationList - {}", resultPermutationList);

            for (Integer[] integers : resultPermutationList) {
                var tmpAdjMatrix = newAdjMatrix.clone();
                for (int j = 0; j < tmpAdjMatrix.length; j++) tmpAdjMatrix[j] = newAdjMatrix[j].clone();
                log.trace("min1Extension k = {} newAdjMatrix - {}", kMinEdges, tmpAdjMatrix);

                log.trace("curr combination - {}", Arrays.toString(integers));
                addEdges(tmpAdjMatrix, integers);
                log.trace("min1Extension k = {} newAdjMatrix after addEdges - {}", kMinEdges, tmpAdjMatrix);

                if (isVerticesExt
                        ? checkVertexEmbedded(adjInMatrix, tmpAdjMatrix, adjInMatrixVerticesDescription, verticesCount)
                        : checkEdgesEmbedded(adjInMatrix, tmpAdjMatrix, adjInMatrixVerticesDescription, verticesCount))
                {
                    log.trace("min1Extension k = {} " +
                            (isVerticesExt ? "checkVertexEmbedded" : "checkEdgesEmbedded") +
                            " is checked", kMinEdges);
                    result.add(tmpAdjMatrix);
                }
            }
            if (result.size() > 0) {
                log.trace("min1Extension k = {} result.size() > 0", kMinEdges);
                break;
            }
        }
        log.trace("min1Extension end");
        return Pair.of(kMinEdges, result);
    }

    @SuppressWarnings("ALL")
    public static Pair<Integer, Collection<Integer[][]>> min1Extension1(Integer[][] adjInMatrix, boolean isVerticesExt) {
        CopyOnWriteArrayList<Integer[][]> result = new CopyOnWriteArrayList<>();

        log.trace("min1Extension isVerticesExt = {}", isVerticesExt);

        log.trace("min1Extension start");
        checkSquareMatrix(adjInMatrix);

        Integer[][] newAdjMatrix = isVerticesExt ? addVertex(adjInMatrix) : adjInMatrix;
        log.trace("min1Extension newAdjMatrix - {}", newAdjMatrix);

        var verticesCount = adjInMatrix.length;
        var emptyPositions = findEmptyPositions(newAdjMatrix);
        log.trace("min1Extension findEmptyPositions - {}", emptyPositions);

        int kMax = isVerticesExt ? (verticesCount * verticesCount - verticesCount - 2) / 2
                : (verticesCount * verticesCount - 3 * verticesCount) / 2;
        log.trace("min1Extension kMax - {}", kMax);

        ArrayList<Vertex> vertices = new ArrayList<>();
        int edgesCount = 0;

        for (int i = 0; i < verticesCount; i++) {
            Vertex v = new Vertex();
            v.num = i;
            vertices.add(v);
        }

        for (int i = 0; i < verticesCount; i++)
            for (int j = 0; j < verticesCount; j++) {
                if (adjInMatrix[i][j] == 1) {
                    vertices.get(i).out++;
                    vertices.get(j).in++;
                    edgesCount++;
                }
            }

        var adjInMatrixVerticesDescription = Pair.of(vertices, edgesCount);
        log.trace("min1Extension adjInMatrixVerticesDescription - {}", adjInMatrixVerticesDescription);

        var kMinEdges = 0;
        for (kMinEdges = isVerticesExt ? 2 : 1; kMinEdges < kMax; kMinEdges++) {
            var tmpK = kMinEdges;
            Consumer<Pair<CopyOnWriteArrayList<Integer[][]>, Integer[]>> processingFun = (Pair<CopyOnWriteArrayList<Integer[][]>, Integer[]> it) -> {
                var tmpAdjMatrix = newAdjMatrix.clone();
                for (int j = 0; j < tmpAdjMatrix.length; j++) tmpAdjMatrix[j] = newAdjMatrix[j].clone();
                log.trace("min1Extension k = {} newAdjMatrix - {}", tmpK, tmpAdjMatrix);

                log.trace("curr combination - {}", Arrays.toString(it.getRight()));
                addEdges(tmpAdjMatrix, it.getRight());
                log.trace("min1Extension k = {} newAdjMatrix after addEdges - {}", tmpK, tmpAdjMatrix);

                if (isVerticesExt
                        ? checkVertexEmbedded(adjInMatrix, tmpAdjMatrix, adjInMatrixVerticesDescription, verticesCount)
                        : checkEdgesEmbedded(adjInMatrix, tmpAdjMatrix, adjInMatrixVerticesDescription, verticesCount))
                {
                    log.trace("min1Extension k = {} " +
                            (isVerticesExt ? "checkVertexEmbedded" : "checkEdgesEmbedded") +
                            " is checked", tmpK);
                    it.getLeft().add(tmpAdjMatrix);
                }
            };
            permutation1(emptyPositions, kMinEdges, 0, verticesCount, new Integer[kMinEdges], result, processingFun).join();

            if (result.size() > 0) {
                log.trace("min1Extension k = {} result.size() > 0", kMinEdges);
                break;
            }
        }
        log.trace("min1Extension end");
        return Pair.of(kMinEdges, result);
    }

    /**
     * Проверка: targetAdjMatrix является мр-1р для sourceAdjMatrix
     * @param sourceAdjMatrix исходная матрица
     * @param targetAdjMatrix целевая матрица
     * @param sourceVerticesDescription описание вершин исходного графа
     * @param sourceVerticesCount количество вершин исходного графа
     * @return targetAdjMatrix является мр-1р для sourceAdjMatrix
     */
    @SuppressWarnings("ALL")
    public static boolean checkEdgesEmbedded(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            Pair<ArrayList<Vertex>, Integer> sourceVerticesDescription,
            Integer sourceVerticesCount
    ) {
        int k = 0;
        for (int i = 0; i < sourceVerticesCount; i++)
            for (int j = 0; j < sourceVerticesCount; j++)
                if (sourceAdjMatrix[i][j] == 1)
                    k++;

        // Trying to embed m in each m2 (m2 = m1 with 1 edge deleted)
        for (int count = 0; count < k; count++) {
            ArrayList<Vertex> vertices = new ArrayList<>();
            int m_out_c = 0;

            for (int i = 0; i < sourceVerticesCount; i++) {
                Vertex v = new Vertex();
                v.num = i;
                vertices.add(v);
            }

            int k1 = 0;
            Integer[][] m2 = new Integer[sourceVerticesCount][];
            for (int i = 0; i < sourceVerticesCount; i++) {
                m2[i] = new Integer[sourceVerticesCount];
                for (int j = 0; j < sourceVerticesCount; j++)
                    if (targetAdjMatrix[i][j] == 1) {
                        if (k1 == count)
                            m2[i][j] = 0;
                        else {
                            m2[i][j] = 1;
                            vertices.get(i).out++;
                            vertices.get(j).in++;
                            m_out_c++;
                        }

                        k1++;
                    } else
                        m2[i][j] = 0;
            }

            Pair<ArrayList<Vertex>, Integer> p = Pair.of(vertices, m_out_c);
            Boolean check = checkMatricesEmbedded(sourceAdjMatrix, m2, sourceVerticesDescription, p, sourceVerticesCount);

            if (!check)
                return false;
        }

        return true;
    }

    /**
     * Проверка: targetAdjMatrix является мв-1р для sourceAdjMatrix
     * @param sourceAdjMatrix исходная матрица
     * @param targetAdjMatrix целевая матрица
     * @param sourceVerticesDescription описание вершин исходного графа
     * @param sourceVerticesCount количество вершин исходного графа
     * @return targetAdjMatrix является мв-1р для sourceAdjMatrix
     */
    @SuppressWarnings("ALL")
    protected static boolean checkVertexEmbedded(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            Pair<ArrayList<Vertex>, Integer> sourceVerticesDescription,
            Integer sourceVerticesCount
    ) {
        // Remember targetAdjMatrix is sourceVerticesCount+1 size
        for (int count = 0; count < sourceVerticesCount + 1; count++) {
            ArrayList<Vertex> vertices = new ArrayList<>();
            int m_out_c = 0;

            for (int i = 0; i < sourceVerticesCount; i++) {
                Vertex v = new Vertex();
                v.num = i;
                vertices.add(v);
            }

            int i = 0, j = 0;
            Integer[][] m2 = new Integer[sourceVerticesCount][];
            for (int i1 = 0; i1 < sourceVerticesCount + 1; i1++)
                if (i1 != count) {
                    m2[i] = new Integer[sourceVerticesCount];
                    j = 0;
                    for (int j1 = 0; j1 < sourceVerticesCount + 1; j1++)
                        if (j1 != count) {
                            m2[i][j] = targetAdjMatrix[i1][j1];
                            if (targetAdjMatrix[i1][j1] == 1) {
                                vertices.get(i).out++;
                                vertices.get(j).in++;
                                m_out_c++;
                            }
                            j++;
                        }
                    i++;
                }

            Pair<ArrayList<Vertex>, Integer> p = Pair.of(vertices, m_out_c);
            boolean check = checkMatricesEmbedded(sourceAdjMatrix, m2, sourceVerticesDescription, p, sourceVerticesCount);

            if (!check) return false;
        }

        return true;
    }

    /**
     * Проверка возможности отображения исходной матрицы в целевую
     * @param sourceAdjMatrix исходная матрица
     * @param targetAdjMatrix целевая матрица
     * @param sourceVerticesDescription описание вершин исходного графа (sourceAdjMatrix)
     * @param targetVerticesDescription описание вершин целевого графа (targetAdjMatrix)
     * @param verticesCount количество вершин исходного графа
     * @return Возможно ли вложить sourceAdjMatrix в targetAdjMatrix
     */
    private static boolean checkMatricesEmbedded(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            Pair<ArrayList<Vertex>, Integer> sourceVerticesDescription,
            Pair<ArrayList<Vertex>, Integer> targetVerticesDescription,
            Integer verticesCount
    ) {
        if (sourceVerticesDescription.getRight() > targetVerticesDescription.getRight()) return false;

        ArrayList<ArrayList<Integer>> potentialVertices = new ArrayList<>(); // For each sourceVerticesDescription.first vertex collect a list of vertices from targetVerticesDescription.first with which it can be matched
        for (int i = 0; i < verticesCount; i++) {
            potentialVertices.add(new ArrayList<>());
            for (int j = 0; j < verticesCount; j++) {
                var sourceVertexI = sourceVerticesDescription.getLeft().get(i);
                var targetVertexJ = targetVerticesDescription.getLeft().get(j);
                if ((sourceVertexI.in <= targetVertexJ.in) & (sourceVertexI.out <= targetVertexJ.out))
                    potentialVertices.get(i).add(j);
            }
        }

        for (int i = 0; i < verticesCount; i++) if (potentialVertices.get(i).size() == 0) return false;

        ArrayList<Integer> ans = new ArrayList<>();
        return checkVerticesMapping(sourceAdjMatrix, targetAdjMatrix, potentialVertices, 0, ans);
    }

    /**
     * Проверка возможности отображения исходной матрицы в целевую
     * @param sourceAdjMatrix исходная матрица
     * @param targetAdjMatrix целевая матрица
     * @param potentialVertices матрица, в которой
     *                          корневой индекс - номер вершины из sourceAdjMatrix,
     *                          вложенный массив Integer - номера вершин из targetAdjMatrix
     * @param vertexIndex текущий номер вершины в исходной матрице
     * @param resVerticesIndeces массив номеров вершин targetAdjMatrix, в которые вкладываются вершины из sourceAdjMatrix
     * @return Возможно ли вложить sourceAdjMatrix в targetAdjMatrix
     */
    private static boolean checkVerticesMapping(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            ArrayList<ArrayList<Integer>> potentialVertices,
            Integer vertexIndex,
            ArrayList<Integer> resVerticesIndeces
    ) {
        for (int count = 0; count < potentialVertices.get(vertexIndex).size(); count++) {
            var a = potentialVertices.get(vertexIndex).get(count);
            if (!resVerticesIndeces.contains(a)) { // This vertex wasn't used yet
                resVerticesIndeces.add(a);

                // Checking edges between mapped vertices
                if (vertexIndex >= 1) {
                    boolean check = true;
                    for (int i = 0; i < resVerticesIndeces.size() - 1; i++) {
                        for (int j = i + 1; j < resVerticesIndeces.size(); j++) {
                            var ansI = resVerticesIndeces.get(i);
                            var ansJ = resVerticesIndeces.get(j);
                            if ((((sourceAdjMatrix[i][j] > targetAdjMatrix[ansI][ansJ])
                                    || (sourceAdjMatrix[j][i] > targetAdjMatrix[ansJ][ansI])))) {
                                resVerticesIndeces.remove(resVerticesIndeces.size() - 1);
                                check = false;
                                break;
                            }
                        }
                        if (!check) break;
                    }

                    if (!check) continue;
                }

                if (vertexIndex == potentialVertices.size() - 1) return true;
                else {
                    if (checkVerticesMapping(
                            sourceAdjMatrix,
                            targetAdjMatrix,
                            potentialVertices,
                            vertexIndex + 1,
                            resVerticesIndeces)
                    ) return true;
				    else
                        resVerticesIndeces.remove(resVerticesIndeces.size() - 1);
                }
            }
        }

        return false;
    }

    protected static void addEdges(Integer[][] adjInMatrix, Integer[] edgesPositions) {
        var verticesCount = adjInMatrix.length;
        for (Integer a : edgesPositions) {
            int x = a / verticesCount;
            int y = a % verticesCount;
            adjInMatrix[x][y] = 1;
        }
    }

    /**
     * Добавить вершину в матрицу смежности
     * @param adjInMatrix матрица смежности
     * @return новая матрица смежности с добавленной вершиной
     */
    private static Integer[][] addVertex(Integer[][] adjInMatrix) {
        var verticesCount = adjInMatrix.length;
        var resultAdjMatrix = new Integer[verticesCount + 1][];
        for (int i = 0; i < verticesCount; i++) {
            var newArr = new Integer[verticesCount + 1];
            System.arraycopy(adjInMatrix[i], 0, newArr, 0, verticesCount);
            newArr[verticesCount] = 0;
            resultAdjMatrix[i] = newArr;
        }
        var newArr = new Integer[verticesCount + 1];
        Arrays.fill(newArr, 0);
        resultAdjMatrix[verticesCount] = newArr;
        return resultAdjMatrix;
    }

    /**
     * Сбор пустых ячеек в матрице смежности по позиции
     * @param adjInMatrix матрица смежности
     * @return массив адресов пустых ячеек
     */
    private static Integer[] findEmptyPositions(Integer[][] adjInMatrix) {
        ArrayList<Integer> resultList = new ArrayList<>();
        var verticesCount = adjInMatrix.length;
        for (int i = 0; i < verticesCount; i++) {
            for (int j = 0; j < verticesCount; j++) {
                var a = adjInMatrix[i][j];
                if (a != 0 || adjInMatrix[j][i] != 0 || i == j) continue;

                resultList.add(i * verticesCount + j);
            }
        }
        Integer[] result = new Integer[resultList.size()];
        resultList.toArray(result);
        return result;
    }

    protected static CompletableFuture<Void> permutation1(
            Integer[] source,
            int k, int sourceStartPos, int verticesCount,
            Integer[] target,
            CopyOnWriteArrayList<Integer[][]> matricesResult,
            Consumer<Pair<CopyOnWriteArrayList<Integer[][]>, Integer[]>> processingFun
    ) {
        if (k == 0){
            if (checkNoSymmetricPlaces(target, verticesCount)) {
                log.trace(Arrays.toString(target));
                processingFun.accept(Pair.of(matricesResult, target.clone()));
            }
            return CompletableFuture.completedFuture(null);
        }
        List<CompletableFuture<Void>> result = new ArrayList<>();
        for (int i = sourceStartPos; i <= source.length-k; i++){
            target[target.length - k] = source[i];
            var j = i;
            result.add(CompletableFuture.runAsync(() ->
                    permutation1(source, k-1, j+1, verticesCount, target, matricesResult, processingFun)));
        }
        CompletableFuture<Void>[] resultArr = new CompletableFuture[result.size()];
        result.toArray(resultArr);
        return CompletableFuture.allOf(resultArr);
    }

    protected static boolean checkNoSymmetricPlaces(Integer[] target, int verticesCount) {
        for (int i = 0; i < target.length; i++) {
            var symmetricElem = (target[i] / verticesCount) + (target[i] % verticesCount) * verticesCount;
            for (int j = i + 1; j < target.length; j++) {
                if (target[j] == symmetricElem) return false;
            }
        }
        return true;
    }

    /**
     * Поиск всех перестановок k элементов в массиве n элементов
     * @param source исходный массив n элементов
     * @param k K кол-во элементов в 1 перестановке
     * @param sourceStartPos стартовая позиция исходного массива
     * @param target результирующий массив перестановок
     */
    protected static void permutation(Integer[] source, int k, int sourceStartPos, int verticesCount, Integer[] target, ArrayList<Integer[]> result) {
        if (k == 0){
            if (checkNoSymmetricPlaces(target, verticesCount)) {
                log.trace(Arrays.toString(target));
                result.add(target.clone());
            }
            return;
        }
        for (int i = sourceStartPos; i <= source.length-k; i++){
            target[target.length - k] = source[i];
            permutation(source, k-1, i+1, verticesCount, target, result);
        }
    }

    /**
     * Валидация матрицы на квадратность
     * @param matrix входящая матрица
     */
    private static void checkSquareMatrix(Integer[][] matrix) {
        if (matrix.length < 1) throw new IllegalArgumentException("adjInMatrix.length < 1");
        //todo check all lines
        if (matrix.length != matrix[0].length) throw new IllegalArgumentException("adjInMatrix is not a adj matrix");
    }
}

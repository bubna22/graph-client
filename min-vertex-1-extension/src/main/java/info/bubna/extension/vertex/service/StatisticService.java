package info.bubna.extension.vertex.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class StatisticService {

    private static final String RESULT_DIR = "statistics";

    static {
        var resultDir = new File(RESULT_DIR);
        if (!resultDir.exists()) resultDir.mkdir();
    }

    public void auditGraphWithoutVertexExt(Integer[][] adjMatrix, int verticesCount, boolean isVerticesExtension) throws IOException {
        var resultVerticesPath = RESULT_DIR + "/" + verticesCount;
        var resultDir = new File(resultVerticesPath);
        if (!resultDir.exists()) {
            resultDir.mkdir();
        }
        var resultFilePath = resultVerticesPath + "/" + (isVerticesExtension ? "vertices_" : "edges_") + "result.txt";
        try (PrintWriter out = new PrintWriter(new FileOutputStream(resultFilePath, true))) {
            out.println(mapAdjMatrixToString(adjMatrix));
        }
    }

    private String mapAdjMatrixToString(Integer[][] adjMatrix) {
        var sb = new StringBuilder();
        for (Integer[] matrix : adjMatrix) {
            sb.append(Arrays.toString(matrix)).append("\n");
        }
        sb.append("\n");
        return sb.toString();
    }
}

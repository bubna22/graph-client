package info.bubna.extension.vertex;

import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

public class FileUtils {

    private static final String RESULT_DIR = "result";

    static {
        var resultDir = new File(RESULT_DIR);
        if (!resultDir.exists()) resultDir.mkdirs();
    }

    private FileUtils() {
    }

    public static void printToFile(
            String digraph6Name,
            Map<Pair<String, Long>, Pair<Integer, Collection<Integer[][]>>> results,
            Integer[][] sourceAdjMatrix,
            Integer verticesCount
    ) throws IOException {
        var resultVerticesPath = RESULT_DIR + "/" + verticesCount;
        var resultDir = new File(resultVerticesPath);
        if (!resultDir.exists()) {
            resultDir.mkdirs();
        }

        try (PrintWriter out = new PrintWriter(resultVerticesPath + "/" + UUID.randomUUID().toString() + ".txt",Charset.forName("Windows-1251"))) {
            out.println("Исходная матрица смежности:\n");

            for (Integer[] integers : sourceAdjMatrix) {
                out.println(Arrays.toString(integers).replace("[", "").replace("]", ""));
            }
            out.println();

            for (Pair<String, Long> extType : results.keySet()) {
                var res = results.get(extType);
                out.println("Время вычислений для " + extType.getLeft() + " (мс): " + extType.getRight());

                out.println("Найдено " + res.getRight().size() + " " + extType.getLeft()
                        + " с минимальным количеством дополнительных ребер = " + res.getLeft() + ":\n");

                for (Integer[][] r : res.getRight()) {
                    for (Integer[] integers : r) {
                        out.println(Arrays.toString(integers).replace("[", "").replace("]", ""));
                    }
                    out.println();
                }
            }
        }
    }
}

package info.bubna.extension.vertex;

import info.bubna.digraph.converter.Digraph6Converter;
import info.bubna.extension.vertex.math.Min1ExtensionAlgorithm;
import info.bubna.extension.vertex.service.StatisticService;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);
    private static final StatisticService statisticService = new StatisticService();
    @SuppressWarnings("ALL")
    public static void main(String[] args) throws IOException {
        var testDigraphs = List.of("&CS`?", "&CS_G", "&CSD?", "&CO`_");
        for (String digraph : testDigraphs) {
            var adjMatrix = Digraph6Converter.digraph6ToAdjMatrix(digraph);
            var results = run(digraph, adjMatrix);
            if (results.keySet().size() > 0)
                FileUtils.printToFile(digraph, results, adjMatrix, adjMatrix.length);
        }
    }

    @SuppressWarnings("ALL")
    public static HashMap<Pair<String, Long>, Pair<Integer, Collection<Integer[][]>>> run(String digraph, Integer[][] adjMatrix)  {

        log.debug("start adj in matrix");
        for (Integer[] integers : adjMatrix) {
            log.debug(Arrays.toString(integers));
        }
        log.debug("end adj in matrix");

        var results = new HashMap<Pair<String, Long>, Pair<Integer, Collection<Integer[][]>>>();

        long verticesExtTimeStart = System.currentTimeMillis();
        var result = Min1ExtensionAlgorithm.min1Extension1(adjMatrix, true);
        long verticesExtTimeEnd = System.currentTimeMillis();

        result.getRight().forEach(it -> {
            log.debug("start matrix");
            for (Integer[] integers : it) {
                log.debug(Arrays.toString(integers));
            }
            log.debug("end matrix");
            log.debug("using digraph - {}", digraph);
        });
        var verticesExtTimeRes = verticesExtTimeEnd - verticesExtTimeStart;
        if (result.getRight().size() > 0) results.put(Pair.of("мв-1р", verticesExtTimeRes), result);
        else {
            try {
                statisticService.auditGraphWithoutVertexExt(adjMatrix, adjMatrix.length, true);
            } catch (IOException e) {
                new RuntimeException(e);
            }
        }

        long edgesExtTimeStart = System.currentTimeMillis();
        var resultEdgesExt = Min1ExtensionAlgorithm.min1Extension1(adjMatrix, false);
        long edgesExtTimeEnd = System.currentTimeMillis();

        resultEdgesExt.getRight().forEach(it -> {
            log.debug("start matrix");
            for (Integer[] integers : it) {
                log.debug(Arrays.toString(integers));
            }
            log.debug("end matrix");
            log.debug("using digraph - {}", digraph);
        });
        var edgesExtTimeRes = edgesExtTimeEnd - edgesExtTimeStart;
        if (resultEdgesExt.getRight().size() > 0) results.put(Pair.of("мр-1р", edgesExtTimeRes), resultEdgesExt);
        else {
            try {
                statisticService.auditGraphWithoutVertexExt(adjMatrix, adjMatrix.length, false);
            } catch (IOException e) {
                new RuntimeException(e);
            }
        }
        return results;
    }
}

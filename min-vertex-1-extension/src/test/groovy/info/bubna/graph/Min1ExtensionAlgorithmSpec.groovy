package info.bubna.graph

import info.bubna.digraph.converter.Digraph6Converter
import info.bubna.extension.vertex.FileUtils
import info.bubna.extension.vertex.Main
import info.bubna.graph.utils.Min1ExtensionAlgorithmExt
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class Min1ExtensionAlgorithmSpec extends Specification {

    Logger log = LoggerFactory.getLogger(Min1ExtensionAlgorithmSpec.class)

    def "integ test"(int verticesCount, String[] digraphsList) {
        expect:
        log.info("integ test for $verticesCount started")
        long startMeasure = System.currentTimeMillis()
        for (String digraph : digraphsList) {
            def adjMatrix = Digraph6Converter.digraph6ToAdjMatrix(digraph)
            def res = Main.run(digraph, adjMatrix)
            if (res.keySet().size() > 0) {
                FileUtils.printToFile(digraph, res, adjMatrix, adjMatrix.length)
            }
        }
        long endMeasure = System.currentTimeMillis()

        log.info("integ test for $verticesCount ended; time measure - ${endMeasure - startMeasure}")

        where:
        verticesCount | digraphsList
        3             | readGraphs("src/test/resources/digraphs_3.txt")
        4             | readGraphs("src/test/resources/digraphs_4.txt")
        5             | readGraphs("src/test/resources/digraphs_5.txt")
        6             | readGraphs("src/test/resources/digraphs_6.txt")
//        7             | readGraphs("src/test/resources/digraphs_7.txt")
//        8             | readGraphs("src/test/resources/digraphs_8.txt")
//        9             | readGraphs("src/test/resources/digraphs_9.txt")
//        10            | readGraphs("src/test/resources/digraphs_10.txt")
    }

    private String[] readGraphs(String fileName) {
        def file = new File(fileName)

        def list2 = []
        file.eachLine {
            list2 << it
        }
        return list2
    }

    def "check permutation()"() {
        setup:
        Integer[] emptyPositions = [0,2,4,6,8,9,10,12,14,16,18,19,24]
        ArrayList<Integer> potentialPositions = [4,9,14,19]
        when:
        def result = new ArrayList<Integer[]>()
        Min1ExtensionAlgorithmExt.permutationTest(emptyPositions, 4, 0, new Integer[4], result)
        then:
        boolean check = true
        for (int j = 0; j < result.size(); j++) {
            def r = result[j]
            check = true
            for (int i = 0; i < r.length; i++) {
                if (!potentialPositions.contains(r[i])) {
                    check = false
                    break
                }
            }
            if (check && r.length == potentialPositions.size()) break
        }
        check
    }

    def "check addEdges()"() {
        setup:
        Integer[][] targetMatrix = [[0,1,0,1,0],[1,0,1,0,0],[0,1,0,1,0],[1,0,1,0,0],[1,1,1,1,0]]
        Integer[][] resultMatrix = [[0,1,0,1,1],[1,0,1,0,1],[0,1,0,1,1],[1,0,1,0,1],[1,1,1,1,0]]
        Integer[] potentialPositions = [4,9,14,19]
        when:
        Min1ExtensionAlgorithmExt.addEdgesTest(targetMatrix, potentialPositions)
        then:
        targetMatrix == resultMatrix
    }

    def "check 4 vertices' cycle embedding"() {
        setup:
        Integer[][] sourceMatrix = [[0,1,0,1],[1,0,1,0],[0,1,0,1],[1,0,1,0]]
        Integer[][] targetMatrix = [[0,1,0,1,1],[1,0,1,0,1],[0,1,0,1,1],[1,0,1,0,1],[1,1,1,1,0]]
        Integer sourceVerticesCount = 4
        when:
        boolean check = Min1ExtensionAlgorithmExt.checkEmbeddedTest(sourceMatrix, targetMatrix, sourceVerticesCount)
        then:
        check
    }

}

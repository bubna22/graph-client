package info.bubna.graph.utils

import info.bubna.extension.vertex.math.Min1ExtensionAlgorithm
import org.apache.commons.lang3.tuple.Pair
import spock.lang.Ignore

@Ignore
public class Min1ExtensionAlgorithmExt extends Min1ExtensionAlgorithm {

    static def checkEmbeddedTest(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetCheckMatrix,
            Integer sourceVerticesCount
    ) {
        return checkVertexEmbedded(
                sourceAdjMatrix,
                targetCheckMatrix,
                calculateDescription(sourceAdjMatrix, sourceVerticesCount),
                sourceVerticesCount
        )
    }

    static def permutationTest(
            Integer[] source,
            Integer k,
            Integer sourceStartPos,
            Integer[] target,
            ArrayList<Integer[]> result
    ) {
        permutation(source, k, sourceStartPos, target, result)
    }

    static def addEdgesTest(Integer[][] sourceAdjMatrix, Integer[] positions) {
        addEdges(sourceAdjMatrix, positions)
    }

    static def calculateDescription(Integer[][] sourceAdjMatrix, Integer verticesCount) {
        ArrayList<Vertex> vertices = new ArrayList<>();
        int edgesCount = 0

        for (int i = 0; i < verticesCount; i++) {
            Vertex v = new Vertex()
            v.num = i
            vertices.add(v)
        }

        for (int i = 0; i < verticesCount; i++)
            for (int j = 0; j < verticesCount; j++) {
                if (sourceAdjMatrix[i][j] == 1) {
                    vertices.get(i).out++
                    vertices.get(j).in++
                    edgesCount++
                }
            }

        return Pair.of(vertices, edgesCount)
    }
}
